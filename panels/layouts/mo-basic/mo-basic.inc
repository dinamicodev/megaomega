<?php

/**
 * @file
 * Defines a simple x-columns grid layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Megaomega basic'),
  'icon' => 'preview.png',
  'category' => t('Omega content layouts'),
  'theme' => 'mo_basic',
  //'css' => '../../../css/layouts/mo-basic/mo-basic.layout.css',
  'regions' => array(
    'top' => t('Top'),
    'first' => t('First'),
    'second' => t('Second'),
    'third' => t('Third'),
    'fourth' => t('Fourth'),
    'fifth' => t('Fifth'),
    'sixth' => t('Sixth'),
    'seventh' => t('Seventh'),
    'eighth' => t('Eighth'),
    'ninth' => t('Ninth'),
    'bottom' => t('Bottom'),
  ),
);

/**
 * Implements hook_preprocess_mo_basic().
 */
function template_preprocess_mo_basic(&$variables) {
  $variables['attributes_array']['class'][] = 'panel-mo-basic';
  $variables['attributes_array']['class'][] = 'panel-display--mo-basic';

  foreach($variables['content'] as $name => $item) {
    $variables['region_attributes_array'][$name]['class'][] = 'mo-basic-region';
    $variables['region_attributes_array'][$name]['class'][] = 'mo-basic-region__' . drupal_clean_css_identifier($name);
  }
}
