<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * MegaOmega theme.
 */

function megaomega_entity_view_alter(&$build, $type) { #dpm($build);

	if (!isset($build['#bundle'])) {
		return;
	}

	foreach ($build as $field_name => $field) {

		if (strpos($field_name, 'product:') === 0) { 
			$build[$field_name]['#prefix'] = '';
			$build[$field_name]['#suffix'] = '';
		}
	}
}